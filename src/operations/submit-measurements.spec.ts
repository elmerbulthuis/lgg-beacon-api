import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("submit-measurement", t => withContext(async context => {
    await initializeMocks();

    const client = context.createApplicationClient({
        basic: { username: "MQ==", password: "dHJ1ZQ==" },
    });

    const response = await client.submitMeasurements({
        parameters: {
            beacon: "AQ==",
        },
        entity() {
            return [
                {
                    ip: "192.168.178.20",
                    pub: "",
                    rtt: 10,
                    stddev: 0.5,
                    type: "udp-data",
                    version: "v0.1.0",
                },
            ];
        },
    });
    assert(response.status === 202);

    const responseEntity = await response.entity();

    t.ok(responseEntity);

    async function initializeMocks() {
        context.servers.auth.registerValidateClientSecretOperation(async incomingRequest => {
            const incomingEntity = await incomingRequest.entity();
            const clientId = incomingRequest.parameters.client;
            const clientSecret = incomingEntity.value;

            return {
                status: 200,
                parameters: {},
                entity() {
                    return { valid: clientId === "MQ==" && clientSecret === "dHJ1ZQ==" };
                },
            };
        });

        await context.services.pgPool.query(`
INSERT INTO public.location( id, created_utc )
VALUES( E'\\x01', '2021-12-02T12:00Z' );

INSERT INTO public.location_label( location, name, value )
VALUES( E'\\x01', 'provider', 'wikipedia' ),
( E'\\x01', 'location', 'example' );

INSERT INTO public.beacon(
    client, location,
    id, version,
    ipv4, ipv6,
    created_utc,
    last_seen_utc
)
VALUES(
    E'\\x31', E'\\x01',
    E'\\x01', 'v0.1.0',
    '127.0.255.250', '2001:db8::8a2e:370:7334',
    '2021-12-02T12:00Z',
    '2021-12-02T12:00Z'
);
`);
    }
}));
