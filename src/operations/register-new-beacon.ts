import * as beaconApi from "@latency.gg/lgg-beacon-oas";
import * as crypto from "crypto";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createRegisterNewBeaconOperation(
    context: application.Context,
): beaconApi.RegisterNewBeaconOperationHandler<application.ServerAuthorization> {
    return async (
        incomingRequest,
        authorization,
        acceptTypes,
    ) => {
        const now = new Date();

        const clientIdBuffer = authorization.basic.client;
        const beaconEntity = await incomingRequest.entity();

        const locationId = beaconEntity.location;
        const locationIdBuffer = Buffer.from(locationId, "base64");

        const publickey = beaconEntity.publickey;
        const publickeyBuffer = Buffer.from(publickey, "base64");

        const beaconIdBuffer = crypto.randomBytes(24);
        const beaconId = beaconIdBuffer.toString("base64");

        const {
            version,
            ipv4, ipv6,
        } = beaconEntity;

        await withPgTransaction(
            context,
            "insert-beacon",
            async pgClient => {
                const result = await pgClient.query(`
INSERT INTO public.beacon (
    id, location, client, public_key,
    ipv4, ipv6,
    version, created_utc, last_seen_utc
)
VALUES (
    $1, $2, $3, $4,
    $5, $6,
    $7, $8, $8
)
;
`, [
                    beaconIdBuffer, locationIdBuffer, clientIdBuffer, publickeyBuffer,
                    ipv4, ipv6,
                    version, now.toISOString(),
                ]);
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() { return { id: beaconId }; },
        };

    };
}
